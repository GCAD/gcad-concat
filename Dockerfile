FROM ubuntu:20.04 AS builder-tools
LABEL maintainer="Otto Valladares <ottov.upenn@gmail.com>"
LABEL version="1.0"
LABEL description="UPENN bctools concat"

ARG DEBIAN_FRONTEND="noninteractive"

RUN apt-get update && apt-get install --no-install-recommends -y groff-base unzip bcftools curl ca-certificates

ADD cmd.sh  /cmd.sh

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
          unzip awscliv2.zip && \
          ./aws/install && \
          rm -f /awscliv2.zip && \
          chmod +x /cmd.sh


# git info
ADD .git/logs/HEAD /HEAD
RUN tail -n 1 /HEAD > /git-log-info
