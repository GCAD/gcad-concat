#!/bin/bash
set -exo pipefail

export SM=$1
export PRJ_ID=$2

export ENABLE_DB=${ENABLE_DB:-"1"}
export API_HOST=${API_HOST:-"https://processing.adgenomics.org"}
export API_TRAK="/v1/track/by-run"
export URI="$API_HOST$API_TRAK"

source <(curl -s "https://bitbucket.org/NIAGADS/vcpa-pipeline/raw/5117b58d079b1fcd3db02fa64124887ddacce165/VCPA/stage0/common_functions.sh" )

s3_results=$(get_s3_results_location $PRJ_ID $SM)
vcf_dir=${vcf_dir:-"VCF/GATK4.1"}

# grab storage
if [ "$GETEBS" == "1" ];then
   if [ ! -z "$EBSSIZE" ];then
     echo $(($EBSSIZE * 1024**3 )) > /TOTAL_SIZE
   else
     # calculate storage
     # sum file sizes
     SZ=$(aws s3 ls "$s3_results/$vcf_dir/" | grep _vcpa1.1_chr | awk '{SUM+=$3}END{printf "%0.f", SUM}')

     echo $((SZ * 2 + (1024**3) )) > /TOTAL_SIZE
   fi

   # wait for ecs-manager to mount EBS
   for i in {1..20};do
      if mountpoint -q -- "/scratch"; then
         break
      fi
      sleep 1m
   done

   if ! mountpoint -q -- "/scratch";then
       echo "Container ERROR"
       exit 1
   fi

   df -h
fi


outfile=${outfile:-${SM}_vcpa1.1.allchr_g.vcf.gz}

aws s3 sync --only-show-error --force-glacier-transfer --exclude '*.*' --include '*_vcpa1.1_chr*'  $s3_results/$vcf_dir /scratch/vcf-in
time bcftools concat --output-type z -o /scratch/$outfile /scratch/vcf-in/${SM}_vcpa1.1_chr{{1..22},X,Y,M}.g.vcf.gz
time bcftools index --tbi /scratch/$outfile
time md5sum /scratch/$outfile > /scratch/$outfile.md5
aws s3 cp --only-show-error /scratch/$outfile $s3_results/$vcf_dir/
aws s3 cp --only-show-error /scratch/$outfile.tbi $s3_results/$vcf_dir/
aws s3 cp --only-show-error /scratch/$outfile.md5 $s3_results/$vcf_dir/
